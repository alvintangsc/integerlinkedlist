#include <iostream>
using namespace std;

struct nodeType
{
	int info;
	nodeType *link;
};

nodeType* buildListForward()
{
	nodeType *first, *newNode, *last;
	int num; // user input
	cout << "Enter a list of integers ending with -999." << endl;
	cout << "Enter: ";
	cin >> num;
	first = NULL;
	last = NULL;

	while( num != -999 )
	{
		newNode = new nodeType;
		newNode->info = num;
		//newNode = NULL; // setting newNode to NULL means we've lost the data
		newNode->link = NULL; // set to null since we are always adding to the end
		if( first == NULL )
		{
			first = newNode;
			last = newNode;
		}
		else
		{
			last->link = newNode;
			last = newNode;
		}

		cout << "Enter: ";
		cin >> num; //read the next number to be added to the end
	} // end while loop
	return first; // need to return the head node!
}

/**
*	Note: This does not check for duplicate items in the list.
*	Pre:	Assumes there are no items in the list and creates a new head node.
*	Post:	Returns the pointer to the first node in the list.
*/
nodeType* buildListBackward()
{
	nodeType *first, *newNode;
	int num;
	cout << "Enter a list of integers ending with -999."
		<< endl;
	cout << "Enter: ";
	cin >> num;
	first = NULL;
	while (num != -999)
	{
		newNode = new nodeType; //create a node
		newNode->info = num; //store the data in newNode
		newNode->link = first; //put newNode at the beginning
		//of the list
		first = newNode; //update the head pointer of
		//the list, that is, first
		cout << "Enter: ";
		cin >> num; //read the next number
	}
	return first;
} //end buildListBackward


/**
* Traverse list and print all items in the list
*/
void printList( nodeType* inHead )
{
	nodeType *current;
	current = inHead;
	while( current != NULL )
	{
		cout << current->info << " "; 
		current = current->link;
	}
	cout << endl;
}


/**
* Traverse list and find the first occurance of a value in the list.
* Note that this does not look for duplicate items.
* Pre:	inSearchItem is a valid integer value; inHead is a valid nodeType pointer or NULL
* Post:	Returns the found node, or NULL if not found.
*/
nodeType* findNode( int inSearchItem, nodeType* inHead )
{
	nodeType *current;
	current = inHead;
	bool isFound = false;

	// Stop searching if we reach the end, or
	// if we have found the item.
	while( current != NULL && !isFound )
	{
		if ( current->info == inSearchItem )
		{
			isFound = true;
		}
		else
		{
			current = current->link;
		}
	}
	return current;
}

/*
*	Pre:	inDeleteNode is a valid nodeType pointer.
*	Post:	The linked list starting at inHead has the inDeleteNode removed.
*/
nodeType* deleteNode( nodeType *inDeleteNode, nodeType *inHead )
{
	nodeType *current;
	current = inHead;

	bool isFound = false;
	// Make sure the node we are trying to delete is a non-NULL pointer
	if ( inDeleteNode != NULL )
	{
		while( current != NULL && !isFound )
		{
			if ( current->link == inDeleteNode )
			{
				current->link = inDeleteNode->link;
			
				if ( inDeleteNode == inHead )
				{
					inHead = inDeleteNode->link;
				}
				
				cout << "Deleting first node holding value: " << inDeleteNode->info << " ..." << endl;

				delete inDeleteNode;
				isFound = true;
			}
			else 
			{
				// move to next item in the list if not found
				current = current->link; 
			}
		}
	}

	return inHead;
}

void main()
{
	// build a list backwards
	//nodeType *head = buildListBackward();
	nodeType *head = buildListForward();
	// print the backwards built list
	printList( head );

	cout << "Locate a node: ";
	int theSearchValue;
	cin >> theSearchValue;

	nodeType *found = findNode( theSearchValue, head );

	if( found != NULL )
	{
		cout << "Node " << found->info << " found!" << endl;
		head = deleteNode( found,  head );
	}
	else
	{
		cout << "Node not found!" << endl;
	}

	printList( head );
}